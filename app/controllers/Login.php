<?php

namespace mvc\controllers;


use mvc\core\Database;
use mvc\core\Controller;
use mvc\models\User;

class Login extends Controller
{
    public function loginForm()
    {
        $this->view('login/form');
    }

    public function login()
    {
        if (isset($_POST['email']) and isset($_POST['password'])) {

            $user = new User();
            $user->setEmail($_POST['email']);
            $user->getUserByEmail();

            if ($user->getPassword() == ($_POST['password'])) {
                $_SESSION['id'] = $_POST['id'];
                header('Location: /mvc/public/index/index');
                exit();
            } else {
                header('Location: /mvc/public/login/loginform');
            }
        }
    }

    public function logout()
    {
        session_destroy();
        header('Location: /mvc/public/login/loginform');
    }

}





