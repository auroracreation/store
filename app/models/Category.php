<?php

namespace mvc\models;

class Category
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;


    public function __construct()
    {
        $this->database = \mvc\core\Database::getInstance();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


    /**
     * Add category to database
     * @param $category
     */
    public function add()
    {
        $this->database->insertRow('category', "(`name`) VALUES(?)", [$this->name]);
    }

    /**
     * Delete category from database
     * @param $category
     */

    public function delete()
    {
        $this->database->deleteRow('category', 'WHERE id = ?', [$this->id]);
    }


    public function load()
    {
        $result = $this->database->getRows('*', 'category');

        return $result;
    }


}